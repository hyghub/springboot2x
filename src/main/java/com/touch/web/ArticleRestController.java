package com.touch.web;

import com.touch.model.AjaxResponse;
import com.touch.model.Article;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/rest" )
public class ArticleRestController {
    @RequestMapping(value = "/article",method = RequestMethod.POST,produces = "application/json")
    public AjaxResponse saveArticle (@RequestBody Article article){
        log.info("saveArticle：{}",article);
        return  AjaxResponse.success(article);
    }
    @RequestMapping(value = "/article/{id}",method = RequestMethod.DELETE,produces = "application/json")
    public AjaxResponse deleteArticle (@PathVariable Long id){
        log.info("deleteArticle：{}",id);
        return AjaxResponse.success(id);
    }
    @RequestMapping(value = "/article/{id}",method = RequestMethod.PUT,produces = "application/json")
    public AjaxResponse updateArticle (@PathVariable Long id){
        log.info("updateArticle：{}",id);
        return AjaxResponse.success(id);
    }
    @RequestMapping(value = "/article/{id}",method = RequestMethod.GET,produces = "application/json")
    public AjaxResponse getArticle (@PathVariable Long id){
        log.info("getArticle：{}",id);
        return AjaxResponse.success(id);
    }
}
